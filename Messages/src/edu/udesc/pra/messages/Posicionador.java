package edu.udesc.pra.messages;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * 
 * Classe responsavel por achar a posicao do registro adicionado no arquivo para indexacao
 *
 */
public class Posicionador {
	
	public static int tamanhoDaLinhaSamplesWin = 405 + 2;
	public static int tamanhoDaLinhaReturnedWin = 17 + 2;
	public static int tamanhoDaLinhaSamplesLin = 405 + 1;
	public static int tamanhoDaLinhaReturnedLin = 17 + 1;
	
	/**
	 * Metodo que calcula a posicao para a correta indexacao dos registros
	 * @param arvoreDeRegistros
	 * @param isReturned
	 * @return
	 * @throws FileNotFoundException
	 */
	public static int acharPosicao(TreeMap<Integer, IndexKey> arvoreDeRegistros, boolean isReturned) throws FileNotFoundException{
		int posicao = 0;
		String caminhoDoArquivo = null;
		Scanner leitor = null;
		
		if(isReturned){
			caminhoDoArquivo = new String("arquivos/returned.pra");
			
			try{
				
				leitor = new Scanner(new FileInputStream(caminhoDoArquivo));
				
				leitor.nextLine();
				
				while(leitor.hasNextLine()){
					leitor.nextLine();
					posicao++;
				}
				
			} finally {
				if(leitor != null) leitor.close();
			}			
		}
		else {
			posicao = arvoreDeRegistros.lastKey() + 1; 
		}
		
		
		return posicao;
	}
	
}
