package edu.udesc.pra.messages;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * 
 * Classe responsavel por indexar os registros do arquivo em uma arvore
 *  
 */
public class Indexador {
	
	/**
	 * Metodo para indexar os registros do arquivo em uma arvore
	 * @param arvoreDeRegistros
	 * @param caminhoDoArquivo
	 */
	public void indexar(TreeMap<Integer, IndexKey> arvoreDeRegistros, String caminhoDoArquivo) {
		
		Scanner leitor = null;
		
		try {
			leitor = new Scanner(new FileInputStream(caminhoDoArquivo));
			String linhaAtual = leitor.nextLine();
			int posicao;
			int linha = 0;
			int tamanhoDaLinha;
			if(GetOS.isWindows()){
				tamanhoDaLinha = linhaAtual.length() + 2;
			}
			else{
				tamanhoDaLinha = linhaAtual.length() + 1;
			}
			
			System.out.println(tamanhoDaLinha);
			Integer id;
			
			while(leitor.hasNextLine()) {
				linhaAtual = leitor.nextLine();
				linha++;
				posicao = linha * tamanhoDaLinha;
				id = Integer.parseInt(linhaAtual.split(";", 2)[0].replaceAll("\\s+",""));
				IndexKey node = new IndexKey(id, posicao, false);
				arvoreDeRegistros.put(id, node);
			}
		} 
		catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}
		finally {
			leitor.close();
		}
		
	}
}
