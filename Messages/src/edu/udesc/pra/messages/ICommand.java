package edu.udesc.pra.messages;

import java.net.Socket;

/**
 * 
 * Interface ICommand que impoe os metodos que os comandos devem implementar
 *
 */
public interface ICommand {
	
	/**
	 * Metodo para a execucao o comando
	 * @param socket
	 * @return
	 */
	public String execute(Socket socket);

}
