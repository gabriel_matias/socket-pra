package edu.udesc.pra.messages;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.TreeMap;

public class FindCommand implements ICommand, Serializable {

	private OrderData data;

	public FindCommand(OrderData data) {
		this.data = data;
	}

	@Override
	public String execute(Socket socket) {
		System.out.println("Iniciando a procura de " + data.getCodigo());

		ObjectInputStream ois = null;
		IndexKey node = null;
		BufferedReader reader = null;
		String caminhoDoArquivo = null;
		String cabecalho;
		String registro;
		String resposta;
		DataOutputStream output = null;
		TreeMap<Integer, IndexKey> arvoreDeRegistros = null;
		boolean isOkay = true;
		try {

			output = new DataOutputStream(socket.getOutputStream());
			
			// Verificacao do arquivo a ser tratado
			if (data.getDescricao().compareTo("returned.pra") == 0) {
				if (GetOS.isWindows()) { // Verifica qual SO esta sendo usado pelo servidor
					ois = new ObjectInputStream(new FileInputStream("arquivos/treeReturned.txt"));
				} else {
					ois = new ObjectInputStream(new FileInputStream("arquivos/treeReturnedLinux.txt"));
				}
				caminhoDoArquivo = new String("arquivos/returned.pra");
			} else if (data.getDescricao().compareTo("SampleSuperstoreSales.pra") == 0) {
				if (GetOS.isWindows()) {
					ois = new ObjectInputStream(new FileInputStream("arquivos/treeSample.txt"));
				} else {
					ois = new ObjectInputStream(new FileInputStream("arquivos/treeSampleLinux.txt"));
				}
				caminhoDoArquivo = new String("arquivos/Sample - Superstore Sales.pra");
			} else {
				// Caso o arquivo passado por parametro nao exista
				resposta = new String("Arquivo inexistente. \n");
				System.out.println("Arquivo inexistente.");
				output.writeBytes(resposta);
				isOkay = false;
			}

			if (isOkay) { // Se nenhum problema ocorreu anteriormente
				arvoreDeRegistros = (TreeMap<Integer, IndexKey>) ois.readObject();
				reader = new BufferedReader(new FileReader(caminhoDoArquivo));

				if (arvoreDeRegistros.containsKey(data.getCodigo())) {

					node = arvoreDeRegistros.get(data.getCodigo());

					if (node.isDeleted()) {
						resposta = new String("Indice inexistente neste arquivo \n");
						System.out.println("Indice inexistente neste arquivo ");
						output.writeBytes(resposta);
						isOkay = false;
					} else {
						cabecalho = reader.readLine();
						if(GetOS.isWindows()){
							reader.skip(node.getPosition() - cabecalho.length() - 2); // Pula para a posicao do registro no arquivo
						}
						else{ // Caso nao seja Windows
							reader.skip(node.getPosition() - cabecalho.length() - 1); // Pula para a posicao do registro no arquivo
						}
						registro = reader.readLine();
						resposta = new String("Registro numero    : " + node.getKey() 
											+ "\nRegistro posicao   : " + node.getPosition() 
											+ "\nRegistro cabecalho : " + cabecalho
											+ "\nRegistro           : " + registro + "\n");
						System.out.println("Enviando resposta...");
						output.writeBytes(resposta);
					}
				} else {
					resposta = new String("Indice inexistente neste arquivo \n");
					System.out.println("Indice inexistente neste arquivo ");
					output.writeBytes(resposta);
					isOkay = false;
				}

			}

		} catch (FileNotFoundException e) {
			System.out.println("Arquivo nao encontrado. Erro: " + e.getMessage());
			isOkay = false;
		} catch (IOException e) {
			System.out.println("Não foi possivel abrir o arquivo. Erro: " + e.getMessage());
			isOkay = false;
		} catch (ClassNotFoundException e) {
			System.out.println("Não foi possivel encontrar a classe. Erro: " + e.getMessage());
			isOkay = false;
		} finally {
			try {
				if (ois != null)
					ois.close();
				if (reader != null)
					reader.close();
				if (output != null)
					output.close();
			} catch (IOException e) {
				System.out.println("Não foi possivel fechar o arquivo. Erro: " + e.getMessage());
				isOkay = false;
			}
		}
		if (isOkay) {
			System.out.println("Executada a procura de " + data.getCodigo());
			return "Comando Procurado :-)";
		} else {
			System.out.println("Operacao nao concluida");
		}
		return "Fail";
	}

}
