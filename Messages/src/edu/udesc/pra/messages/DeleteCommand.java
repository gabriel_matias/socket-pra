package edu.udesc.pra.messages;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.TreeMap;

/**
 * 
 * Classe responsavel por implementar o comando de deletar um registro do arquivo
 * 
 */
public class DeleteCommand implements ICommand, Serializable {

	private OrderData data;

	public DeleteCommand(OrderData data) {
		this.data = data;
	}

	@Override
	public String execute(Socket socket) {
		System.out.println("Iniciando eliminacao de " + data.getCodigo());

		ObjectInputStream oisWin = null;
		ObjectOutputStream oosWin = null;
		ObjectInputStream oisLin = null;
		ObjectOutputStream oosLin = null;
		IndexKey node = null;
		String resposta;
		DataOutputStream output = null;
		TreeMap<Integer, IndexKey> arvoreDeRegistros = null;
		boolean isOkay = true;
		boolean isReturned = true;
		try {
			
			output = new DataOutputStream(socket.getOutputStream());
			
			// Verificacao do arquivo a ser tratado
			if (data.getDescricao().compareTo("returned.pra") == 0) {

				oisWin = new ObjectInputStream(new FileInputStream("arquivos/treeReturned.txt"));
				oisLin = new ObjectInputStream(new FileInputStream("arquivos/treeReturnedLinux.txt"));
			} else if (data.getDescricao().compareTo("SampleSuperstoreSales.pra") == 0) {

				oisWin = new ObjectInputStream(new FileInputStream("arquivos/treeSample.txt"));
				oisLin = new ObjectInputStream(new FileInputStream("arquivos/treeSampleLinux.txt"));
				isReturned = false;
			} else {
				// Caso o arquivo passado por parametro nao exista
				resposta = new String("Arquivo inexistente. \n");
				System.out.println("Arquivo inexistente.");
				output.writeBytes(resposta);
				isOkay = false;
			}

			if (isOkay) { // Se nenhum problema ocorreu anteriormente
				arvoreDeRegistros = (TreeMap<Integer, IndexKey>) oisWin.readObject();
				if(oisWin != null) oisWin.close();
				
				if (arvoreDeRegistros.containsKey(data.getCodigo())) { // Verifica se o ID existe

					node = arvoreDeRegistros.get(data.getCodigo());

					if (node.isDeleted()) {
						resposta = new String("Indice inexistente neste arquivo \n");
						System.out.println("Indice inexistente neste arquivo ");
						output.writeBytes(resposta);
						isOkay = false;
					} else {
						// Se nao estiver deletado
						// Coloca o no como deletado e atualiza o arquivo da arvore do Windows e do Linux
						node.setDeleted(true);
						arvoreDeRegistros.put(data.getCodigo(), node);
						
						if(isReturned){
							oosWin = new ObjectOutputStream(new FileOutputStream("arquivos/treeReturned.txt"));
						}
						else{
							oosWin = new ObjectOutputStream(new FileOutputStream("arquivos/treeSample.txt"));
						}
						oosWin.writeObject(arvoreDeRegistros);
						
						arvoreDeRegistros = (TreeMap<Integer, IndexKey>) oisLin.readObject();
						if(oisLin != null) oisLin.close();
						
						node = arvoreDeRegistros.get(data.getCodigo());
						node.setDeleted(true);
						arvoreDeRegistros.put(data.getCodigo(), node);
						
						if(isReturned){
							oosLin = new ObjectOutputStream(new FileOutputStream("arquivos/treeReturnedLinux.txt"));
						}
						else{
							oosLin = new ObjectOutputStream(new FileOutputStream("arquivos/treeSampleLinux.txt"));
						}
						
						oosLin.writeObject(arvoreDeRegistros);

						resposta = new String("Registro numero: "+data.getCodigo()+" foi excluido com sucesso! \n");
						System.out.println("Enviando resposta...");
						output.writeBytes(resposta);
					}
				} else {
					resposta = new String("Indice inexistente neste arquivo \n");
					System.out.println("Indice inexistente neste arquivo ");
					output.writeBytes(resposta);
					isOkay = false;
				}

			}

		} catch (FileNotFoundException e) {
			System.out.println("Arquivo nao encontrado. Erro: " + e.getMessage());
			isOkay = false;
		} catch (IOException e) {
			System.out.println("Não foi possivel abrir o arquivo. Erro: " + e.getMessage());
			isOkay = false;
		} catch (ClassNotFoundException e) {
			System.out.println("Não foi possivel encontrar a classe. Erro: " + e.getMessage());
			isOkay = false;
		} finally {
			try {
				if (oosWin != null)
					oosWin.close();
				if (oosLin != null)
					oosLin.close();
				if (output != null)
					output.close();
			} catch (IOException e) {
				System.out.println("Não foi possivel fechar o arquivo. Erro: " + e.getMessage());
				isOkay = false;
			}
		}
		if (isOkay) {
			System.out.println("Executada a eliminacao de " + data.getCodigo());
			return "Comando Eliminado :-(";
		} else {
			System.out.println("Operacao nao concluida");
		}
		return "Fail";

	}

}
