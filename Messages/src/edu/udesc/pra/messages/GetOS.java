package edu.udesc.pra.messages;

/**
 * 
 * Classe responsavel por detectar o Sistema Operacionar utilizado
 * Necessario para correta indexacao, adicao e procura nos arquivos independentemente do sistema operacional utilizado
 * Caracteres de quebra de linha nos arquivos sao tratados de forma diferente em SOs diferentes
 *
 */
public class GetOS {
	
	private static String os = System.getProperty("os.name");
	
	/**
	 * Verifica se o SO utilizado e Windows ou nao
	 * @return
	 */
	public static boolean isWindows(){
		if(GetOS.os.contains("Windows")){
			return true;
		}
		return false;
	}
	
	
}
