package edu.udesc.pra.messages;

import java.io.Serializable;

/**
 * 
 * Classe usada como dado passado do cliente para o servidor com atributos utilizados pelos comandos
 *
 */
public class OrderData implements Serializable {
	private int codigo;
	private String descricao;
	private String[] campos;
	
	
	public String[] getCampos() {
		return campos;
	}

	public void setCampos(String[] campos) {
		this.campos = campos;
	}

	public OrderData() {
		super();
	}

	public OrderData(int codigo, String descricao) {
		super();
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	

}
