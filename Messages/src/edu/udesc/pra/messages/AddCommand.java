package edu.udesc.pra.messages;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.TreeMap;

/**
 * 
 * Classe responsavel por implementar o comando de adicionar um registro no arquivo
 * 
 */
public class AddCommand implements ICommand, Serializable{
	
	private OrderData data;
	
	public AddCommand(OrderData data) {
		this.data = data;
	}

	@Override
	public String execute(Socket socket) {
		System.out.println("Iniciando a adicao de " + data.getCodigo());
		
		ObjectInputStream oisWin = null;
		ObjectOutputStream oosWin = null;
		ObjectInputStream oisLin = null;
		ObjectOutputStream oosLin = null;
		IndexKey node = null;
		IndexKey newNode = null;
		BufferedWriter outFile = null;
		String resposta;
		StringBuilder registro = null;
		String[] campos;
		String caminhoDoArquivo = null;
		DataOutputStream output = null;
		TreeMap<Integer, IndexKey> arvoreDeRegistros = null;
		boolean isOkay = true;
		boolean isReturned = true;
		int[] tamanhoDosCamposReturned = {8, 8};
		int[] tamanhoDosCamposSales = {6, 8, 10, 14, 14, 9, 8, 14, 9, 10, 13, 22, 21, 21, 16, 16, 30, 98, 17, 19, 10};
		
		try {
			
			output = new DataOutputStream(socket.getOutputStream());
			
			// Verificacao do arquivo a ser tratado
			if (data.getDescricao().compareTo("returned.pra") == 0) {

				oisWin = new ObjectInputStream(new FileInputStream("arquivos/treeReturned.txt"));
				oisLin = new ObjectInputStream(new FileInputStream("arquivos/treeReturnedLinux.txt"));
				caminhoDoArquivo = new String("arquivos/returned.pra");
			} else if (data.getDescricao().compareTo("SampleSuperstoreSales.pra") == 0) {

				oisWin = new ObjectInputStream(new FileInputStream("arquivos/treeSample.txt"));
				oisLin = new ObjectInputStream(new FileInputStream("arquivos/treeSampleLinux.txt"));
				isReturned = false;
				caminhoDoArquivo = new String("arquivos/Sample - Superstore Sales.pra");
			} else {
				// Caso o arquivo passado por parametro nao exista
				resposta = new String("Arquivo inexistente. \n");
				System.out.println("Arquivo inexistente.");
				output.writeBytes(resposta);
				isOkay = false;
			}
			
			if(isOkay){ // Se nenhum problema ocorreu anteriormente
				arvoreDeRegistros = (TreeMap<Integer, IndexKey>) oisWin.readObject();
				if(oisWin != null) oisWin.close();
				registro = new StringBuilder();
				
				if(isReturned){ // apenas verifica id existente para o returned
					if(arvoreDeRegistros.containsKey(data.getCodigo())){ //verifica se o codigo ja existe
						node = arvoreDeRegistros.get(data.getCodigo());
						if(node.isDeleted() == false){ // se existir, verifica se nao esta deletado
							resposta = new String("Indice ja esta em uso. Tente outro. \n");
							System.out.println("Indice ja esta em uso. ");
							output.writeBytes(resposta); // Se não tiver, nao pode ser usado
							isOkay = false;
						}
						else{ // se existir e estiver deletado, pode ser usado
							campos = data.getCampos();
							
							// Arruma tamanho do campo passado pelo cliente
							for(int i = 0; i < campos.length; i++){
								if(campos[i].length() < tamanhoDosCamposReturned[i]){
									campos[i] = String.format("%" + -(tamanhoDosCamposReturned[i]) + "s", campos[i]);
								}
								else{
									campos[i] = campos[i].substring(0, tamanhoDosCamposReturned[i]);
								}
								
							}
							
							// Cria a string registro concatenando os campos
							for(int i = 0; i < campos.length - 1; i++){
								registro.append(campos[i] + ";");
							}
							registro.append(campos[campos.length - 1]);
							
							outFile = new BufferedWriter(new FileWriter(caminhoDoArquivo, true));
							
							// Escreve registro no arquivo
							outFile.write(registro.toString());
							outFile.newLine();
							if(outFile != null) outFile.close();
							
							oosWin = new ObjectOutputStream(new FileOutputStream("arquivos/treeReturned.txt"));
							
							// Calcula posicao do registro adicionado para colocar no no da arvore
							int posicao = Posicionador.acharPosicao(arvoreDeRegistros, isReturned) * Posicionador.tamanhoDaLinhaReturnedWin;
							
							// Cria novo no e adiciona na arvore
							newNode = new IndexKey(data.getCodigo(), posicao, false);
							arvoreDeRegistros.put(data.getCodigo(), newNode);
							
							// Salva arvore do usada quando o SO e windows
							oosWin.writeObject(arvoreDeRegistros);
							
							// Repete para a arvore usada no SO Linux
							arvoreDeRegistros = (TreeMap<Integer, IndexKey>) oisLin.readObject();
							if(oisLin != null) oisLin.close();
							
							posicao = Posicionador.acharPosicao(arvoreDeRegistros, isReturned) * Posicionador.tamanhoDaLinhaReturnedLin;
							newNode = new IndexKey(data.getCodigo(), posicao, false);
							
							arvoreDeRegistros.put(data.getCodigo(), newNode);
							oosLin = new ObjectOutputStream(new FileOutputStream("arquivos/treeReturnedLinux.txt"));
							oosLin.writeObject(arvoreDeRegistros);
							
						}
					}
				}
				else{ // Caso o arquivo nao seja o returned.pra
					campos = data.getCampos();
					
					// Calcula o index a ser usado no registro
					int indice = arvoreDeRegistros.lastKey() + 1;
					
					data.setCodigo(indice);
					
					campos[0] = Integer.toString(indice);
					
					for(int i = 0; i < campos.length; i++){
						if(campos[i].length() < tamanhoDosCamposSales[i]){
							campos[i] = String.format("%" + -(tamanhoDosCamposSales[i]) + "s", campos[i]);
						}
						else{
							campos[i] = campos[i].substring(0, tamanhoDosCamposSales[i]);
						}
						
					}
					
					
					
					for(int i = 0; i < campos.length - 1; i++){
						registro.append(campos[i] + ";");
					}
					registro.append(campos[campos.length - 1]);
					
					outFile = new BufferedWriter(new FileWriter(caminhoDoArquivo, true));
					
					outFile.write(registro.toString());
					outFile.newLine();
					
					if(outFile != null) outFile.close();
					
					oosWin = new ObjectOutputStream(new FileOutputStream("arquivos/treeSample.txt"));
					
					int posicao = Posicionador.acharPosicao(arvoreDeRegistros, isReturned) * Posicionador.tamanhoDaLinhaSamplesWin;
					
					newNode = new IndexKey(data.getCodigo(), posicao, false);
					arvoreDeRegistros.put(data.getCodigo(), newNode);
					
					oosWin.writeObject(arvoreDeRegistros);
					
					arvoreDeRegistros = (TreeMap<Integer, IndexKey>) oisLin.readObject();
					if(oisLin != null) oisLin.close();
					
					posicao = Posicionador.acharPosicao(arvoreDeRegistros, isReturned) * Posicionador.tamanhoDaLinhaSamplesLin;
					newNode = new IndexKey(data.getCodigo(), posicao, false);
					
					arvoreDeRegistros.put(data.getCodigo(), newNode);
					oosLin = new ObjectOutputStream(new FileOutputStream("arquivos/treeSampleLinux.txt"));
					oosLin.writeObject(arvoreDeRegistros);
					
				}
				
				if(isOkay){
					resposta = new String("Registro numero: "+data.getCodigo()+" foi adicionado com sucesso! \n");
					System.out.println("Enviando resposta...");
					output.writeBytes(resposta);
				}				
				
			}

		} catch (FileNotFoundException e) {
			System.out.println("Arquivo nao encontrado. Erro: " + e.getMessage());
			isOkay = false;
		} catch (IOException e) {
			System.out.println("Não foi possivel abrir o arquivo. Erro: " + e.getMessage());
			isOkay = false;
		} catch (ClassNotFoundException e) {
			System.out.println("Não foi possivel encontrar a classe. Erro: " + e.getMessage());
			isOkay = false;
		} finally {
			try {
				if (oosWin != null)
					oosWin.close();
				if (oosLin != null)
					oosLin.close();
				if (output != null)
					output.close();
			} catch (IOException e) {
				System.out.println("Não foi possivel fechar o arquivo. Erro: " + e.getMessage());
				isOkay = false;
			}
		}
		if (isOkay) {
			System.out.println("Executada a adicao de " + data.getCodigo());
			return "Comando Adicionado :-)";
		} else {
			System.out.println("Operacao nao concluida");
		}
		return "Fail";
	}
	
}
