package edu.udesc.pra.messages;

import java.io.Serializable;

/**
 * 
 * Classe utilizada como no da arvore indexada
 *
 */
public class IndexKey implements Serializable {
	
	private int key;
	private int position;
	
	public IndexKey(int key, int position, boolean deleted) {
		this.key = key;
		this.position = position;
		this.deleted = deleted;
	}
	
	private boolean deleted = false;
	
	public boolean isDeleted() {
		return deleted;
	}
	
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public int getKey() {
		return key;
	}
	public void setKey(int key) {
		this.key = key;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	
	
	

}
