package edu.udesc.pra.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.CharBuffer;

import edu.udesc.pra.messages.ICommand;

// Implementa o design patter singleton, ou seja, nao � poss�vel fazer new nessa classe tem que usar o getInstance
/**
 * 
 * Classe que cria a comunicacao com o servidor
 *
 */
public class Communicate {
	
	private static Communicate instance = null;
	private static final String HOST = "localhost";
	private static final int PORT = 3400;
	
	private Communicate() {
		
	}
	
	/**
	 * Metodo que torna impossivel a criacao de mais de uma instancia dessa classe 
	 * @return
	 */
	public static Communicate getInstance() {
		if( instance == null ) {
			instance = new Communicate();
		}
		return instance;
	}
	
	/**
	 * Envia o comando para o servidor e aguarda a resposta
	 * @param cmd
	 * @return
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public String send(ICommand cmd) throws UnknownHostException, IOException {
		 Socket client = new Socket(HOST, PORT);
		 OutputStream os = client.getOutputStream();
		 ObjectOutputStream oos = new ObjectOutputStream(os);
		 BufferedReader inFromServer = new BufferedReader(new InputStreamReader(client.getInputStream()));
		 oos.writeObject(cmd);
		 
		 StringBuilder returned = new StringBuilder();
		 String line;
		 while( (line = inFromServer.readLine()) != null) {
			 returned.append(line+"\n");
		 }
		 
		 return returned.toString();
		
	}

}
