package edu.udesc.pra.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.util.Scanner;

import edu.udesc.pra.messages.FindCommand;
import edu.udesc.pra.messages.GetOS;
import edu.udesc.pra.messages.OrderData;

/**
 * Possui a tela principal do cliente onde os comandos serao digitados pelo usuario
 * 
 * A resposta do servidor é mostrada ao usuario atraves desta tela
 *
 */
public class MainScreen {

	public static void main(String[] args) {
		
		OrderData data = new OrderData();
		String command = new String();
		Scanner reader = new Scanner(System.in);
		String[] campos = null;
		
		while(true){
			
			System.out.print("***********************************************************************************\n");
			System.out.print("* Digite seu comando passando o nome do arquivo como parametro e o id do registro *\n");
			System.out.print("* Exemplo: find returned.pra 59937                                                *\n");
			System.out.print("* Exemplo: find SampleSuperstoreSales.pra 1                                       *\n");
			System.out.print("* Arquivos existentes: returned.pra SampleSuperstoreSales.pra                     *\n");
			System.out.print("* Para sair digite: quit                                                          *\n");
			System.out.print("***********************************************************************************\n\n");
			
			command = reader.nextLine();
			
			String[] arrayCmd = command.split(" ");
			
			if(arrayCmd[0].compareTo("quit") == 0){
				reader.close();
				break;	
			}
			else if(arrayCmd.length != 3){
				System.out.println("Numero de argumentos errado. ");
			}
			else if(arrayCmd[0].compareTo("find") == 0){
				data.setCodigo(Integer.valueOf(arrayCmd[2]));
				data.setDescricao(arrayCmd[1]);
				FindScreen find = new FindScreen();
				try {
					String returned = find.confirm(data);
					System.out.println(returned);
				} catch (UnknownHostException e) {
					System.out.println("Host desconhecido. Erro: "+e.getMessage());
				} catch (IOException e) {
					System.out.println("Nao pode realizar a conexao. Erro: "+e.getMessage());
				}
			}
			else if(arrayCmd[0].compareTo("delete") == 0){
				data.setCodigo(Integer.valueOf(arrayCmd[2]));
				data.setDescricao(arrayCmd[1]);
				DeleteScreen delete = new DeleteScreen();
				try {
					String returned = delete.confirm(data);
					System.out.println(returned);
				} catch (UnknownHostException e) {
					System.out.println("Host desconhecido. Erro: "+e.getMessage());
				} catch (IOException e) {
					System.out.println("Nao pode realizar a conexao. Erro: "+e.getMessage());
				}
			}
			else if(arrayCmd[0].compareTo("add") == 0){
				data.setCodigo(Integer.valueOf(arrayCmd[2]));
				data.setDescricao(arrayCmd[1]);
				data.setCampos(Campo.criar(arrayCmd));
				AddScreen add = new AddScreen();
				try {
					String returned = add.confirm(data);
					System.out.println(returned);
				} catch (UnknownHostException e) {
					System.out.println("Host desconhecido. Erro: "+e.getMessage());
				} catch (IOException e) {
					System.out.println("Nao pode realizar a conexao. Erro: "+e.getMessage());
				}
			}
			else {
				System.out.println("Comando não encontrado, tente novamente.");
			}
			System.out.println("Aperte a tecla ENTER para continuar...");
			reader.nextLine();
		}
	}

}
