package edu.udesc.pra.client;

import java.util.Scanner;

/**
 * 
 * Classe responsavel pela criacao dos campos dos arquivos
 *
 */
public class Campo {
	
	/**
	 * Metodo para a criacao do array de String que sera enviado para o servidor como os campos do registro 
	 * @param arrayCmd
	 * @return
	 */
	public static String[] criar(String [] arrayCmd) {
		
		String[] campos;
		
		System.out.println("Digite cada campo: ");
		System.out.println("*Obs.: Tamanho maximo do campo esta entre parenteses. ");
		if(arrayCmd[1].compareTo("returned.pra") == 0){
			campos = new String[2];
			
			campos[0] = leCampo("Order ID", 8);
			campos[1] = leCampo("Status", 8);
			
		}
		else{
			campos = new String[21];
			
			campos[0] = leCampo("Row ID", 6);
			campos[1] = leCampo("Order ID", 8);
			campos[2] = leCampo("Order Date", 10);
			campos[3] = leCampo("Order Priority", 14);
			campos[4] = leCampo("Order Quantity", 14);
			campos[5] = leCampo("Sales", 9);
			campos[6] = leCampo("Discount", 8);
			campos[7] = leCampo("Ship Mode", 14);
			campos[8] = leCampo("Profit", 9);
			campos[9] = leCampo("Unit Price", 10);
			campos[10] = leCampo("Shipping Cost", 13);
			campos[11] = leCampo("Customer Name", 22);
			campos[12] = leCampo("Province", 21);
			campos[13] = leCampo("Region", 21);
			campos[14] = leCampo("Customer Segment", 16);
			campos[15] = leCampo("Product Category", 16);
			campos[16] = leCampo("Product Sub-Category", 30);
			campos[17] = leCampo("Product Name", 98);
			campos[18] = leCampo("Product Container", 17);
			campos[19] = leCampo("Product Base Margin", 19);
			campos[20] = leCampo("Ship Date", 10);
		}

		return campos;
	}
	
	/**
	 * Metodo para requisitar ao usuario que entre com a descricao do campo
	 * @param campo
	 * @param tamMax
	 * @return
	 */
	private static String leCampo(String campo, int tamMax) {
		Scanner reader = new Scanner(System.in);
		System.out.print(campo + "(" + tamMax + "): ");
		return reader.nextLine();
	}
	
}
