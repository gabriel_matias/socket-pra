package edu.udesc.pra.client;

import java.io.IOException;
import java.net.UnknownHostException;

import edu.udesc.pra.messages.DeleteCommand;
import edu.udesc.pra.messages.ICommand;
import edu.udesc.pra.messages.OrderData;

/**
 * 
 * Classe responsavel por criar e enviar o comando de deletar para o servidor
 *
 */
public class DeleteScreen {

	/**
	 * Metodo que confirma o envio do comando de deletar para o servidor e recebe a resposta
	 * @param data
	 * @return
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	 public String confirm(OrderData data ) throws UnknownHostException, IOException {
		 ICommand del = new DeleteCommand(data);
		 
		 Communicate com = Communicate.getInstance();
		 String returned = com.send( del );
		 
		 return returned;		 
	 }
}
