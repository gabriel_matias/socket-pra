package edu.udesc.pra.server;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.TreeMap;

import edu.udesc.pra.messages.GetOS;
import edu.udesc.pra.messages.ICommand;
import edu.udesc.pra.messages.IndexKey;
import edu.udesc.pra.messages.Indexador;
import edu.udesc.pra.messages.OrderData;

/**
 * 
 * Classe responsavel por implementar o servidor e executa-lo
 *
 */
public class Server extends Thread {
	private static final boolean FOREVER = true;
	private static final int PORT = 3400;
	
	public Server() {
		
		
	}
	
	/**
	 * Cria o servidor e fica no loop do recebimento
	 * @see java.lang.Thread#run()
	 */
	public void run() {
		try {
			ServerSocket server = new ServerSocket(PORT);	
			do {
				receiver(server);
			} while( FOREVER );
			
		} catch (IOException e) {
			System.out.println("Erro ->" + e.getMessage());
		}
		

	}

	/**
	 * Recebe as comunicacoes e manda executar um comando
	 */
	private void receiver(ServerSocket server) {
		try {
			Socket socket = server.accept();
			InputStream input = socket.getInputStream();
			ObjectInputStream oInput = new ObjectInputStream(input);
			
			ICommand cmd = (ICommand) oInput.readObject();
			Thread cmdRunner = new Thread( new Executor(cmd, socket) );
			
			cmdRunner.start();
			
		} catch (Exception e) {
			System.out.println("Erro ->" + e.getMessage());
		}
	}
	
	/**
	 * Metodo principal da classe Server
	 * Inicia o processo de recebimento de comunicacoes
	 * @param args
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void main(String args[]) throws FileNotFoundException, IOException {
		
		// Codigo para indexar as arvores novamente - codigo utilizado apenas em caso de problemas
//		Indexador indexador = new Indexador();
//		ObjectOutputStream oosReturned;
//		if(GetOS.isWindows()){
//			oosReturned = new ObjectOutputStream(new FileOutputStream("arquivos/treeReturned.txt"));
//		}
//		else{
//			oosReturned = new ObjectOutputStream(new FileOutputStream("arquivos/treeReturnedLinux.txt"));
//		}
//		
//		TreeMap<Integer, IndexKey> arvoreDeRegistros = new TreeMap<>();
//		indexador.indexar(arvoreDeRegistros, "arquivos/returned.pra");
//		oosReturned.writeObject(arvoreDeRegistros);
//		oosReturned.close();
//		
//		ObjectOutputStream oosSample;
//		if(GetOS.isWindows()){
//			oosSample = new ObjectOutputStream(new FileOutputStream("arquivos/treeSample.txt"));
//		}
//		else{
//			oosSample = new ObjectOutputStream(new FileOutputStream("arquivos/treeSampleLinux.txt"));
//		}
//		arvoreDeRegistros = new TreeMap<>();
//		indexador.indexar(arvoreDeRegistros, "arquivos/Sample - Superstore Sales.pra");
//		oosSample.writeObject(arvoreDeRegistros);
//		oosSample.close();
		
		// **********************************************************************************************
		
		
		Thread server = new Server();
		server.run();
	}
}
