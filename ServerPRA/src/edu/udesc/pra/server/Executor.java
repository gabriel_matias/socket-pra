package edu.udesc.pra.server;

import java.net.Socket;

import edu.udesc.pra.messages.ICommand;

/**
 * 
 * Classe responsavel por executar o comando na Thread
 * @see java.lang.Thread#run()
 *
 */
public class Executor implements Runnable {
    private ICommand cmd;
    private Socket socket;
    
    public Executor(ICommand cmd, Socket socket) {
    	this.cmd = cmd;
    	this.socket = socket;
    }
    
	@Override
	public void run() {
		cmd.execute(this.socket);
	}

}
